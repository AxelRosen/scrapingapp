class AddFieldsToDeparture < ActiveRecord::Migration[7.0]
  def change
    change_table(:departures) do |t|
      t.column :departure_time, :date_time
      t.column :arrival_time, :date_time
      t.column :destination, :string
      t.column :check_in_terminal, :string
      t.column :gate_status, :string
    end
  end
end
