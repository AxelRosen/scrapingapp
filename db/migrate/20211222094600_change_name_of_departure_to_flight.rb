class ChangeNameOfDepartureToFlight < ActiveRecord::Migration[7.0]
  def change
    rename_table('departures', 'flights')
  end
end
