namespace :scraping do
  desc "TODO"
  task scrape: :environment do
    response =  Net::HTTP.get("www.cph.dk", "/api/FlightInformation/GetFlightInfoTable?direction=D&dateQuery=&timeQuery=0&language=da")

    # puts response
    parsedResponse = JSON.parse(response)
    puts "parsed response"
    puts parsedResponse

    parsedResponse.each do |flight|
      Flight.create(departure_time: flight["Time"], from:"Copenhagen", destination: flight["Destination"], check_in_terminal:flight["Terminal"], gate_status: flight["Gate"])
    end
  end

end
