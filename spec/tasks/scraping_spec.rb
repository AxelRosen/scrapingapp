require 'rails_helper'
Rails.application.load_tasks

describe "Runs tasks" do

    it "check that the scraper runs and creates a flight" do
        jsonResponse = <<~JSON
        [
            {
                "Time": "14:55",
                "Delayed": false,
                "ExpectedTime": "14:55",
                "Airline": "IBERIA EXPRESS",
                "Destination": "Madrid",
                "Iata": "I23730",
                "Gate": "",
                "Terminal": "",
                "Status": "",
                "SmsLink": "",
                "FlightId": "10659941",
                "DestinationName": "Madrid, Spanien - Adolfo Suarez Madrid-Barajas Airport",
                "IataDestination": "MAD"
            }
        ]
        JSON
        stub_request(:get, "http://www.cph.dk/api/FlightInformation/GetFlightInfoTable?direction=D&dateQuery=&timeQuery=0&language=da").to_return(body: jsonResponse)
        Rake::Task["scraping:scrape"].invoke()
        expect(Flight.count).to eq(1)
    end
end