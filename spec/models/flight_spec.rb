require 'rails_helper'

RSpec.describe Flight, type: :model do

    it "checks that a Flight can be created" do
        @date_time_now =  DateTime.now
        @date_time_tomorrow =  DateTime.tomorrow
        
        @flight = Flight.create(from: "Copenhagen", departure_time: @date_time_now, arrival_time: @date_time_tomorrow, destination: "Madrid", check_in_terminal: "T2", gate_status:"A12 To Gate")
        expect(@flight.from).to eq("Copenhagen")
        expect(@flight.departure_time).to eq(@date_time_now)
        expect(@flight.arrival_time).to eq(@date_time_tomorrow)
        expect(@flight.destination).to eq("Madrid")
        expect(@flight.check_in_terminal).to eq("T2")
        expect(@flight.gate_status).to eq("A12 To Gate")
    end
end